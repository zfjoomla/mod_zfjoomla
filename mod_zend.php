<?php
/**
 * ZFJoomla
 * Zend Framework for Joomla
 * Red Black Tree LLC
 *
 *  Render a Zend View as a module within Joomla
 *
 * @author rbsolutions (rbsoultions.us@gmail.com)
 * @copyright (c) 2011 Red Black Tree LLC
 * @category JoomlZend
 * @package ModZend
 * @link http://joomlazend.rbsolutions.us
 * @version $Id:$
 */
defined ('_JEXEC') or
    die('Restricted Access');
// setup the zend access
require_once(getcwd().DIRECTORY_SEPARATOR.'components'.DIRECTORY_SEPARATOR
        .'com_zend'.DIRECTORY_SEPARATOR.'config.php');


require_once( dirname(__FILE__).DS.'helper.php' );
$zend = modZendHelper::getZend($params);
require(JModuleHelper::getLayoutPath('mod_zend'));
