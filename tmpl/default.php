<?php
/**
 * ZFJoomla
 * Zend Framework for Joomla
 * Red Black Tree LLC
 *
 *  Render a Zend View as a module within Joomla
 *
 * @author rbsolutions (rbsoultions.us@gmail.com)
 * @copyright (c) 2011 Red Black Tree LLC
 * @category JoomlZend
 * @package ModZend
 * @link http://joomlazend.rbsolutions.us
 * @version $Id:$
 */
defined ('_JEXEC') or
    die('Restricted Access');
    echo $zend;

