<?php
/**
 * ZFJoomla
 * Zend Framework for Joomla
 * Red Black Tree LLC
 *
 *  Render a Zend View as a module within Joomla
 *
 * @author rbsolutions (rbsoultions.us@gmail.com)
 * @copyright (c) 2011 Red Black Tree LLC
 * @category JoomlZend
 * @package ModZend
 * @link http://joomlazend.rbsolutions.us
 * @version $Id:$
 */

/**
 * Helper class for Zend module
 *
 * @package ModZend
 * @subpackage Modules
 */
class modZendHelper
{
    /**
     * getZend
     *
     * Retrieves the zend action
     *
     * @param array $params An object containing the module parameters
     * @access public
     */
    function getZend( $params )
    {
        // Ensure library/ is on include_path
        set_include_path(implode(PATH_SEPARATOR, array(
                '.',
                ROOT_DIR . "/library",
                ROOT_DIR . '/application/models',
                get_include_path(),
        )));


        require_once 'Zend/Application.php';

        //Create application, bootstarp, and run
        $application = new Zend_Application(
                APPLICATION_ENV,
                APPLICATION_PATH . '/configs/application.ini'
        );
        try {
            $application->bootstrap();
        } catch(Exception $ex) {
            die('Error loading bootstrap');
        }
        $front= Zend_Controller_Front::getInstance();
        $front->setRequest(new Zend_Controller_Request_Http());
        $front->setResponse(new Zend_Controller_Response_Http());
        // get the joomla document
        $document =& JFactory::getDocument();
        if (Zend_Registry::isRegistered('view')) {
            $view = Zend_Registry::get('view');
        } else {
            $view = new Zend_View();
            // add jquery and other helpers
            $view->addHelperPath(APPLICATION_PATH.DIRECTORY_SEPARATOR.'..'.DIRECTORY_SEPARATOR
                    .'library'.DIRECTORY_SEPARATOR.'ZendX'.DIRECTORY_SEPARATOR
                    .'JQuery'.DIRECTORY_SEPARATOR.'View'.DIRECTORY_SEPARATOR
                    .'Helper', 'ZendX_JQuery_View_Helper');
            $view->addHelperPath(APPLICATION_PATH.DIRECTORY_SEPARATOR.'..'.DIRECTORY_SEPARATOR
                    .'library'.DIRECTORY_SEPARATOR.'Core'.DIRECTORY_SEPARATOR
                    .'View'.DIRECTORY_SEPARATOR.'Helper', 'Core_View_Helper');
                    ZendX_JQuery_View_Helper_JQuery::enableNoConflictMode();
            $view->jQuery()->enable();
            $view->jQuery()->uiEnable();
            $view->jQuery()->setUiVersion('1.8'); 
            $view->jQuery()->setVersion('1.4.2');
            $view->headLink()->appendStylesheet(APPLICATION_URL.'/public/css/jquery-ui-1.8.6.custom.css');
            $document->addScriptDeclaration("</script>".$view->JQuery()."<script>",'text/javascript');
        }
		
        // run the action
        $result = $view->action($params->get('zaction'),$params->get('zcontroller'),$params->get('zmodule'),array('addparams'=>$params->get('addparams')));
		
        // step through the stylesheets
        foreach($view->headLink() as $link) {
            $document->addStyleSheet($link->href,$link->type,$link->media);
        }
        // step through the styles and addd them to the stack
        foreach($view->headStyle() as $style) {
            $document->addStyleDeclaration($style->content);
        }
        
        // step through the scripts and add them to the stack
        foreach($view->headScript() as $script) {
            if($script->source ==NULL) {
                // add the script file
                $document->addScript($script->attributes['src']);
            } else {
                // add the script
                $document->addScriptDeclaration($script->source,$script->type);
            }
        }
		foreach($view->JQuery()->getOnLoadActions() as $action) {
			$document->addScriptDeclaration($action,'text/javascript');
		}
		return $result;
    }
}